<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});





// Remove Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'gallery-menu' => __( 'Gallery Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2);


/* ACF add options page */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page( array(

        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'icon_url' => 'dashicons-images-alt2',
        'position' => 65

    ) );

}

// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  number_format($params['total']) . ' Products';
    return $output;
}, 10, 2 );

// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

//Gravity Scroll to confirmation
add_filter( 'gform_confirmation_anchor', '__return_true' );



function cptui_register_my_cpts_glass_tile() {

    /**
     * Post Type: Glass Tile.
     */

    $labels = array(
        "name" => __( 'Glass Tile', '' ),
        "singular_name" => __( 'Glass Tile', '' ),
        "menu_name" => __( 'Glass Tile Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Glass Tile', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/glass-tile/glass-tile-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "glass_tile", $args );
}

add_action( 'init', 'cptui_register_my_cpts_glass_tile' );



function cptui_register_my_cpts_natural_stone() {

    /**
     * Post Type: Natural Stone.
     */

    $labels = array(
        "name" => __( 'Natural Stone', '' ),
        "singular_name" => __( 'Natural Stone', '' ),
        "menu_name" => __( 'Natural Stone Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Natural Stone', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/natural-stone/natural-stone-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "natural_stone", $args );
}

add_action( 'init', 'cptui_register_my_cpts_natural_stone' );



//Automatically registers new BB Modules from the theme
function register_custom_modules(){
    if ( class_exists( 'FLBuilder' ) ) {
        $dir=dirname(__FILE__)."/fl-builder/custom-modules";
        $files=scandir($dir);
        foreach($files as $k=>$v){
            $file=$dir."/".$v;
            if(is_dir($file) && file_exists($file."/index.php")){
                include_once($file."/index.php");
            }
        }
    }
}

add_action("init","register_custom_modules");

add_image_size("logo_slider",false,52);
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');



// //Yoast SEO Breadcrumb link - Changes for plp pages only

remove_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_cusrom_override_yoast_breadcrumb_trail' );

function wpse_cusrom_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/hardwood-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/carpet-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl-tile/luxury-vinyl-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/laminate-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-porcelain-tile/',
            'text' => 'Ceramic & Porcelain Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-porcelain-tile/ceramic-porcelain-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'glass_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/',
            'text' => 'Glass Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/glass-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }
    

    return $links;
}